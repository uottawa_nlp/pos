#Use Python 2.7
import itertools

EXPECTED_OUTPUT_FILE = 'POS_tagger_expected_output.txt'
ACTUAL_OUTPUT_FILE   = 'outputs/POS_results.txt'
TAGS_FREQUENCY_FILE   = 'outputs/tags_frequency.txt'

def parse_output_file(file_loc):
    sentences = []
    with open(file_loc, 'r') as f:
        for line in f:
            token_tags = line.split(' ')
            sentence = []
            for token_tag in token_tags:
                token_tag = token_tag.split('_')
                token = ''.join(token_tag[0:len(token_tag)])
                tag = token_tag[len(token_tag) - 1].strip()
                sentence.append((token, tag))
            sentences.append(sentence)
    return sentences

def compute_accuracy(ao_sentences, eo_sentences):
    total_tokens = 0
    matches = 0
    for ao_sentence, eo_sentence in itertools.izip(ao_sentences, eo_sentences):
        total_tokens += len(ao_sentence)
        for ao_token_tag, eo_token_tag in \
            itertools.izip(ao_sentence, eo_sentence):
            if ao_token_tag[0] == eo_token_tag[0] \
               and ao_token_tag[1] == eo_token_tag[1]:
               matches += 1
    return (float(matches*100)/float(total_tokens))

def compute_tag_frequency(ao_sentences):
    tags_frequency = {}
    for ao_sentence in ao_sentences:
        for token_tag in ao_sentence:
            token = token_tag[0]
            tag = token_tag[1]
            if tag in tags_frequency:
                tags_frequency[tag] = tags_frequency[tag] + 1
            else:
                tags_frequency[tag] = 1
    return tags_frequency


def write_dict_to_file(dict, file_loc):
    with open(file_loc, 'w') as f:
        for k,v in dict.iteritems():
            f.write("%s : %s\n" % (str(k), str(v)))

ao_sentences = parse_output_file(ACTUAL_OUTPUT_FILE)
eo_sentences = parse_output_file(EXPECTED_OUTPUT_FILE)

accuracy = compute_accuracy(ao_sentences, eo_sentences)
tags_frequency = compute_tag_frequency(ao_sentences)
write_dict_to_file(tags_frequency, TAGS_FREQUENCY_FILE)

print("\nPOS tagging accuracy for the whole corpus is %s\n" % (accuracy))
print("Frequency of POS tags .... \n")
print(tags_frequency)
