if [ ! -d "outputs" ]
then
    mkdir outputs
else
    rm -rf outputs
    mkdir outputs
fi
if [ ! -d "twitie-tagger" ]
then
    echo "twitie-tagger(GATE Twitter Model) contents is not present. Exiting ... "
    return 1
fi

cd twitie-tagger

echo "Using GATE Twitter model to find POS tags ... "
java -jar twitie_tag.jar models/gate-EN-twitter.model ../POS_tagger_input.txt > ../outputs/POS_results.txt
echo "POS_tagger_actual_output.txt file created ... "
echo ""

cd ..

echo "First 20 tagged sentences by the tagger ... "
echo ""
head -n 20 outputs/POS_results.txt
echo ""

echo "Computing the accuracy by comparing with the expected solution file ..."
if [ ! -e "outputs/POS_results.txt" ] || \
   [ ! -e "POS_tagger_expected_output.txt" ]
then
    echo "POS_tagger_expected_output.txt or(and) POS_results.txt file is not present ... "
else
    python compute_accuracy.py
fi
