# Steps to execute the code in your local computer 

1. Ensure you have python 2.7
2. Run the following command to execute the code,
>
    bash POS_tagger.sh
3. The required output files and few other output files would get generated in the `outputs` directory. Also, the answers to the questions would be printed in STDOUT.
